# OpenStack Control Plane at CERN

This is an umbrella chart managing the OpenStack control plane deployments at
CERN.

## Pre-Requisites

First get a Kubernetes cluster up and running, [check here](https://clouddocs.web.cern.ch/containers/quickstart.html).

Make sure you also [deploy Helm](https://clouddocs.web.cern.ch/containers/tutorials/helm.html).

## Deployment

```bash
helm repo list
fluxcd          https://charts.fluxcd.io  
...

helm install fluxcd/helm-operator --namespace flux --name helm-operator   --version 0.4.0 --values helm-operator-values.yaml 
helm install fluxcd/flux --namespace flux --name flux --values flux-values.yaml --set git.url=https://gitlab.cern.ch/helm/releases/openstack --set git.branch=strigazi-heattn
```

## Compute nodes

### 513-c-ip907

- ns/atc-poc-513-c-ip907
- releases/atc-poc-513-c-ip907/values.yaml
- secrets/tn/tn-secret.yaml

```
$ kubectl apply -f namespaces/ats-poc-513-c-ip907.yaml 
namespace/ats-poc-513-c-ip907 created

$ kubectl -n ats-poc-513-c-ip907 create -f charts/openstack-compute-nodes/configmap-golang-neutron-dhcp.yaml 
configmap/golang-neutron-dhcp created


$ kubectl label node i69227911048033.cern.ch  node-role.kubernetes.io/ats-poc-513-c-ip907-linuxbridge=enabled
node/i69227911048033.cern.ch labeled
$ kubectl label node i69227911048033.cern.ch  node-role.kubernetes.io/ats-poc-513-c-ip907-compute=enabled

$ helm install -n ats-poc-513-c-ip907 ats-poc-513-c-ip907 ./charts/openstack-compute-nodes/ -f releases/ats-poc-513-c-ip907/values.yaml -f secrets/tn/tn-secrets.yaml  
WARNING: Kubernetes configuration file is group-readable. This is insecure. Location: /afs/cern.ch/user/s/strigazi/ws/clusters/tn1/config
NAME: ats-poc-513-c-ip907
LAST DEPLOYED: Thu Dec  8 15:18:05 2022
NAMESPACE: ats-poc-513-c-ip907
STATUS: deployed
REVISION: 1
TEST SUITE: None


```


```

$ kubectl -n tn exec -it nova-api-osapi-6bbcb6cc67-55xcr -- bash
Defaulted container "nova-osapi" out of: nova-osapi, init (init)
bash-4.2$
bash-4.2$ nova-manage cell_v2 list_cells
bash-4.2$ nova-manage cell_v2 discover_hosts --cell_uuid 2c79567c-c62a-4c41-9dad-4174e93153d2
bash-4.2$ nova-manage cell_v2 list_hosts --cell_uuid 2c79567c-c62a-4c41-9dad-4174e93153d2 
bash-4.2$ nova-manage cell_v2 list_hosts --cell_uuid 2c79567c-c62a-4c41-9dad-4174e93153d2  | grep i69227911048033.cern.ch
| tn1_be_001 | 2c79567c-c62a-4c41-9dad-4174e93153d2 | i69227911048033.cern.ch |


openstack --os-compute-api-version=2.53 aggregate create tn1-513-c-ip907
openstack --os-compute-api-version=2.53 aggregate add host tn1-513-c-ip907 i69227911048033.cern.ch
openstack --os-compute-api-version=2.53 aggregate set --property availability_zone=tn1-513 tn1-513-c-ip907
openstack --os-placement-api-version=1.2 resource provider aggregate set --aggregate 493e31a1-36c7-423f-96e1-f7a2fad21228  2fca50b1-3800-4969-a440-734fa1d0099d
openstack --os-compute-api-version=2.53 aggregate set --property cell_name=tn1_be_001 tn1-513-c-ip907


```
