glance:
  images:
    tags:
      glance_api: docker.io/openstackhelm/glance:rocky-ubuntu_xenial
      glance_registry: docker.io/openstackhelm/glance:rocky-ubuntu_xenial
      dep_check: 'quay.io/stackanetes/kubernetes-entrypoint:v0.3.1'
  storage: pvc
  volume:
    class_name: openebs-hostpath-1
    size: 10Gi
  conf:
    glance:
      oslo_messaging_rabbit:
        rabbit_ha_queues: false
  network:
    api:
      ingress:
        public: true
        classes:
          namespace: "tn"
          cluster: "nginx-cluster"
        annotations:
          kubernetes.io/ingress.class: traefik
          traefik.frontend.entryPoints: "https"
      external_policy_local: false
  dependencies:
    static:
      api:
        jobs: []
        services: []
      bootstrap:
        jobs: []
        services: []
      registry:
        jobs: []
        services: []
  endpoints:
    identity:
      name: keystone
      auth:
        glance:
          role: admin
          username: svcglanc
          project_name: services
          user_domain_name: Default
          project_domain_name: Default
      hosts:
        default: keystone.cern.ch
        internal: keystone.cern.ch
      host_fqdn_override:
        default: keystone.cern.ch
      path:
        default: /v3
      scheme:
        default: https
      port:
        api:
          default: 443
          internal: 443
    image:
      name: glance
      hosts:
        default: glance-api
        public: openstack-glance-tn1
      host_fqdn_override:
        default: openstack-glance-tn1.cern.ch
        public:
          host: openstack-glance-tn1.cern.ch
      scheme:
        default: 'http'
      port:
        api:
          default: 9292
          public: 80
    oslo_cache:
      hosts:
        default: "cci-mem-ay87ah.cern.ch,cci-mem-bk2r76.cern.ch,cci-mem-c8ywae.cern.ch,cci-mem-dtjwt8.cern.ch,cci-mem-e4i8n5.cern.ch,cci-mem-f2h79o.cern.ch"
      host_fqdn_override:
        default: "cci-mem-ay87ah.cern.ch,cci-mem-bk2r76.cern.ch,cci-mem-c8ywae.cern.ch,cci-mem-dtjwt8.cern.ch,cci-mem-e4i8n5.cern.ch,cci-mem-f2h79o.cern.ch"
    oslo_db:
      auth:
        admin:
          username: admin
        glance:
          username: glance
    oslo_messaging:
      auth:
        admin:
          username: rabbit
        glance:
          username: rabbit
      path: /glance
  manifests:
    deployment_registry: false
    job_bootstrap: false
    job_clean: false
    job_db_init: false
    job_db_sync: false
    job_image_repo_sync: false
    job_ks_endpoints: false
    job_ks_service: false
    job_ks_user: false
    job_metadefs_load: false
    job_storage_init: false
    job_rabbit_init: false
    pdb_api: false
    pdb_registry: false
    pod_rally_test: false
    secret_rabbitmq: false

heat:
  images:
    tags:
      heat_db_sync: docker.io/openstackhelm/heat:rocky-ubuntu_xenial
      heat_api: docker.io/openstackhelm/heat:rocky-ubuntu_xenial
      heat_cfn: docker.io/openstackhelm/heat:rocky-ubuntu_xenial
      heat_cloudwatch: docker.io/openstackhelm/heat:rocky-ubuntu_xenial
      heat_engine: docker.io/openstackhelm/heat:rocky-ubuntu_xenial
      heat_engine_cleaner: docker.io/openstackhelm/heat:rocky-ubuntu_xenial
      dep_check: 'quay.io/stackanetes/kubernetes-entrypoint:v0.3.1'
  conf:
    heat:
      DEFAULT:
        deferred_auth_method: trusts
        heat_stack_user_role: "heat_stack_user"
        max_nested_stack_depth: "10"
        max_nova_api_microversion: "2.15"
        max_stacks_per_tenant: "10000"
        max_template_size: "5242880"
        stack_domain_admin: heat_admin
        stack_user_domain_name: heat
        transport_url: null
        trusts_delegated_roles: Member
        max_resources_per_stack: "-1"
        action_retry_limit: "10"
        client_retry_limitit: "10"
        engine_life_check_timeout: "600"
        enable_stack_abandon: "True"
        rpc_poll_timeout: "600"
        executor_thread_pool_size: "18"
        rpc_response_timeout: "600"
      keystone_authtoken:
        auth_type: password
        auth_version: v3
        service_token_roles_required: true
  network:
    api:
      ingress:
        public: true
        classes:
          namespace: "tn"
          cluster: "nginx-cluster"
        annotations:
          kubernetes.io/ingress.class: traefik
          traefik.frontend.entryPoints: "https"
      external_policy_local: false
    cfn:
      ingress:
        public: true
        classes:
          namespace: "tn"
          cluster: "nginx-cluster"
        annotations:
          kubernetes.io/ingress.class: traefik
          traefik.frontend.entryPoints: "https"
      external_policy_local: false
  dependencies:
    static:
      api:
        jobs: []
        services: []
      cfn:
        jobs: []
        services: []
      cloudwatch:
        jobs: []
        services: []
      engine:
        jobs: []
        services: []
      engine_cleaner:
        jobs: []
        services: []
      purge_deleted:
        jobs: []
        services: []
      trusts:
        jobs: []
        services: []
  endpoints:
    identity:
      name: keystone
      auth:
        heat:
          role: admin
          username: svcheat
          project_name: services
          user_domain_name: Default
          project_domain_name: Default
        heat_trustee:
          role: admin
          username: heat_admin
          user_domain_name: heat
      hosts:
        default: keystone.cern.ch
        internal: keystone.cern.ch
      host_fqdn_override:
        default: keystone.cern.ch
      path:
        default: /v3
      scheme:
        default: https
      port:
        api:
          default: 443
          internal: 443
    orchestration:
      name: heat
      hosts:
        default: heat-api
        public: openstack-heat-tn1
      host_fqdn_override:
        default: openstack-heat-tn1.cern.ch
    cloudformation:
      name: heat-cfn
      hosts:
        default: heat-cfn
        public: openstack-heatcfn-tn1
      host_fqdn_override:
        default: openstack-heatcfn-tn1.cern.ch
    oslo_cache:
      hosts:
        default: "cci-mem-ay87ah.cern.ch,cci-mem-bk2r76.cern.ch,cci-mem-c8ywae.cern.ch,cci-mem-dtjwt8.cern.ch,cci-mem-e4i8n5.cern.ch,cci-mem-f2h79o.cern.ch"
      host_fqdn_override:
        default: "cci-mem-ay87ah.cern.ch,cci-mem-bk2r76.cern.ch,cci-mem-c8ywae.cern.ch,cci-mem-dtjwt8.cern.ch,cci-mem-e4i8n5.cern.ch,cci-mem-f2h79o.cern.ch"
    oslo_db:
      auth:
        admin:
          username: admin
        heat:
          username: heat
    oslo_messaging:
      auth:
        admin:
          username: rabbit
        heat:
          username: rabbit
      path: /heat
  manifests:
    configmap_bin: true
    configmap_etc: true
    deployment_api: true
    deployment_cfn: true
    deployment_cloudwatch: false
    deployment_engine: true
    ingress_api: true
    ingress_cfn: true
    ingress_cloudwatch: false
    job_bootstrap: false
    job_db_init: false
    job_db_sync: false
    job_db_drop: false
    job_image_repo_sync: false
    job_ks_endpoints: false
    job_ks_service: false
    job_ks_user_domain: false
    job_ks_user_trustee: false
    job_ks_user: false
    job_rabbit_init: false
    pdb_api: false
    pdb_cfn: false
    pdb_cloudwatch: false
    pod_rally_test: false
    network_policy: false
    secret_db: false
    secret_ingress_tls: false
    secret_keystone: false
    secret_rabbitmq: false
    service_api: true
    service_cfn: true
    service_cloudwatch: false
    service_ingress_api: true
    service_ingress_cfn: true
    service_ingress_cloudwatch: false
    statefulset_engine: false

libvirt:
  labels:
    agent:
      libvirt:
        node_selector_key: openstack-compute-node
        node_selector_value: enabled
  images:
    tags:
      libvirt: gitlab-registry.cern.ch/cloud/loci-images/nova:19.0.1-10
      dep_check: 'quay.io/stackanetes/kubernetes-entrypoint:v0.3.1'
  network:
    backend:
      - linuxbridge
  conf:
    ceph:
      enabled: false
  dependencies:
    dynamic:
      common:
        local_image_registry:
          jobs: []
          services: []
    static:
      libvirt:
        pods: []
        services: []
      image_repo_sync:
        pods: []
        services: []
  manifests:
    configmap_bin: true
    configmap_etc: true
    daemonset_libvirt: true
    job_image_repo_sync: false
    network_policy: false

magnum:
  images:
    tags:
      magnum_api: rochaporto/magnum:train
      magnum_conductor: rochaporto/magnum:train
      dep_check: 'quay.io/stackanetes/kubernetes-entrypoint:v0.3.1'
  conf:
    magnum:
      DEFAULT:
        transport_url: null
      keystone_authtoken:
        auth_type: password
        auth_version: v3
        service_token_roles_required: true
      certificates:
        cert_manager_type: barbican
      cluster:
        nodes_affinity_policy: 'soft-anti-affinity'
      conductor:
        workers: 1
      trust:
        trustee_domain_id: 4cb76a98145b11e793ae92361f002671
        trustee_domain_name: heat
        trustee_domain_admin_id: 33612461b088461f87e6e004c6273036
        trustee_domain_admin_name: ''
        cluster_user_trust: True
  network:
    api:
      ingress:
        public: true
        classes:
          namespace: "tn"
          cluster: "nginx-cluster"
        annotations:
          kubernetes.io/ingress.class: traefik
          traefik.frontend.entryPoints: "https"
      external_policy_local: false
  dependencies:
    static:
      api:
        jobs: []
        services: []
      conductor:
        jobs: []
        services: []
  endpoints:
    identity:
      name: keystone
      auth:
        magnum:
          role: admin
          username: svcmag
          project_name: services
          user_domain_name: Default
          project_domain_name: Default
      hosts:
        default: keystone.cern.ch
        internal: keystone.cern.ch
      host_fqdn_override:
        default: keystone.cern.ch
      path:
        default: /v3
      scheme:
        default: https
      port:
        api:
          default: 443
          internal: 443
    container_infra:
      name: magnum
      hosts:
        default: magnum-api
        public: openstack-magnum-tn1
      host_fqdn_override:
        default: openstack-magnum-tn1.cern.ch
      scheme:
        default: 'http'
      port:
        api:
          default: 9511
          public: 80
    oslo_cache:
      hosts:
        default: "cci-mem-ay87ah.cern.ch,cci-mem-bk2r76.cern.ch,cci-mem-c8ywae.cern.ch,cci-mem-dtjwt8.cern.ch,cci-mem-e4i8n5.cern.ch,cci-mem-f2h79o.cern.ch"
      host_fqdn_override:
        default: "cci-mem-ay87ah.cern.ch,cci-mem-bk2r76.cern.ch,cci-mem-c8ywae.cern.ch,cci-mem-dtjwt8.cern.ch,cci-mem-e4i8n5.cern.ch,cci-mem-f2h79o.cern.ch"
    oslo_db:
      auth:
        admin:
          username: admin
        magnum:
          username: magnum
    oslo_messaging:
      auth:
        admin:
          username: rabbit
        magnum:
          username: rabbit
      path: /magnum
  manifests:
    ingress_api: true
    job_bootstrap: false
    job_db_init: false
    job_db_sync: false
    job_image_repo_sync: false
    job_ks_endpoints: false
    job_ks_service: false
    job_ks_user: false
    job_ks_user_domain: false
    job_rabbit_init: false
    pdb_api: false
    secret_rabbitmq: false

nova:
  images:
    tags:
      nova_api: gitlab-registry.cern.ch/cloud/loci-images/nova:19.0.1-10
      nova_cell_setup: gitlab-registry.cern.ch/cloud/loci-images/nova:19.0.1-10
      nova_compute: gitlab-registry.cern.ch/cloud/loci-images/nova:19.0.1-10
      nova_compute_ironic: 'docker.io/kolla/ubuntu-source-nova-compute-ironic:ocata'
      nova_compute_ssh: gitlab-registry.cern.ch/cloud/loci-images/nova:19.0.1-10
      nova_conductor: gitlab-registry.cern.ch/cloud/loci-images/nova:19.0.1-10
      nova_consoleauth: gitlab-registry.cern.ch/cloud/loci-images/nova:19.0.1-10
      nova_db_sync: gitlab-registry.cern.ch/cloud/loci-images/nova:19.0.1-10
      nova_novncproxy: gitlab-registry.cern.ch/cloud/loci-images/nova:19.0.1-10
      nova_novncproxy_assets: 'docker.io/kolla/ubuntu-source-nova-novncproxy:ocata'
      nova_placement: gitlab-registry.cern.ch/cloud/loci-images/nova:19.0.1-10
      nova_scheduler: gitlab-registry.cern.ch/cloud/loci-images/nova:19.0.1-10
      nova_spiceproxy: gitlab-registry.cern.ch/cloud/loci-images/nova:19.0.1-10
      dep_check: 'quay.io/stackanetes/kubernetes-entrypoint:v0.3.1'
  network:
    backend:
      - linuxbridge
    osapi:
      ingress:
        public: true
        classes:
          namespace: "tn"
          cluster: "nginx-cluster"
        annotations:
          kubernetes.io/ingress.class: traefik
          traefik.frontend.entryPoints: "https"
      external_policy_local: false
    metadata:
      ingress:
        public: true
        classes:
          namespace: "tn"
          cluster: "nginx-cluster"
        annotations:
          kubernetes.io/ingress.class: traefik
          traefik.frontend.entryPoints: "https"
      external_policy_local: false
    placement:
      ingress:
        public: true
        classes:
          namespace: "tn"
          cluster: "nginx-cluster"
        annotations:
          kubernetes.io/ingress.class: traefik
          traefik.frontend.entryPoints: "https"
      external_policy_local: false
    novncproxy:
      ingress:
        public: true
        classes:
          namespace: "tn"
          cluster: "nginx-cluster"
        annotations:
          kubernetes.io/ingress.class: traefik
          traefik.frontend.entryPoints: "https"
      external_policy_local: false
  dependencies:
    static:
      api:
        jobs: []
        services: []
      api_metadata:
        jobs: []
        services: []
      bootstrap:
        services: []
      cell_setup:
        jobs: []
        services: []
        pod: []
      compute:
        jobs: []
        services: []
      compute_ironic:
        jobs: []
        services: []
      conductor:
        jobs: []
        services: []
      consoleauth:
        jobs: []
        services: []
      novncproxy:
        jobs: []
        services: []
      scheduler:
        jobs: []
        services: []
      service_cleaner:
        jobs: []
        services: []
        pod: []
      server:
        jobs: []
        services: []
  conf:
    ceph:
      enabled: false
    logging:
      logger_root:
        #level: DEBUG
        level: INFO
        handlers: stdout
      logger_nova:
        #level: DEBUG
        level: INFO
        handlers:
          - stdout
        qualname: nova
      logger_amqp:
        #level: DEBUG
        level: INFO
        handlers: stderr
        qualname: amqp
      logger_amqplib:
        #level: DEBUG
        level: INFO
        handlers: stderr
        qualname: amqplib
      logger_eventletwsgi:
        #level: DEBUG
        level: INFO
        handlers: stderr
        qualname: eventlet.wsgi.server
      logger_sqlalchemy:
        #level: DEBUG
        level: INFO
        handlers: stderr
        qualname: sqlalchemy
    nova:
      DEFAULT:
        vif_plugging_is_fatal: "false"
        vif_plugging_timeout: "0"
        metadata_listen: "0.0.0.0"
        metadata_listen_port: "8775"
      neutron:
        service_metadata_proxy: "false"
        metadata_proxy_shared_secret: ""
      oslo_messaging_rabbit:
        rabbit_ha_queues: false
    software:
      apache2:
        binary: httpd
        start_parameters: -DFOREGROUND
        conf_dir: /etc/httpd/conf.d
  endpoints:
    identity:
      name: keystone
      auth:
        nova:
          role: admin
          username: svcnova
          project_name: services
          user_domain_name: Default
          project_domain_name: Default
        placement:
          role: admin
          username: svcplace
          project_name: services
          user_domain_name: Default
          project_domain_name: Default
      hosts:
        default: keystone.cern.ch
        internal: keystone.cern.ch
      host_fqdn_override:
        default: keystone.cern.ch
      path:
        default: /v3
      scheme:
        default: https
      port:
        api:
          default: 443
          internal: 443
    compute:
      name: nova
      hosts:
        default: nova-api
        public: openstack-novaapi-tn1
      host_fqdn_override:
        default: openstack-novaapi-tn1.cern.ch
        public:
          host: openstack-novaapi-tn1.cern.ch
      path:
        default: "/v2.1/%(tenant_id)s"
      scheme:
        default: 'http'
      port:
        api:
          default: 8774
          public: 80
        novncproxy:
          default: 6080
    compute_metadata:
      name: nova
      ip:
        # IF blank, set clusterIP and metadata_host dynamically
        ingress: null
      hosts:
        default: nova-metadata
        public: openstack-novameta-tn1
      host_fqdn_override:
        default: openstack-novameta-tn1.cern.ch
      path:
        default: /
      scheme:
        default: 'http'
      port:
        metadata:
          default: 8775
          public: 80
    compute_novnc_proxy:
      name: nova
      hosts:
        default: nova-novncproxy
        public: openstack-novanovnc-tn1
      host_fqdn_override:
        default: openstack-novanovnc-tn1.cern.ch
      path:
        default: /vnc_auto.html
      scheme:
        default: 'http'
      port:
        novnc_proxy:
          default: 6080
          public: 80
    placement:
      name: placement
      hosts:
        default: placement-api
        public: openstack-placementapi-tn1
      host_fqdn_override:
        default: openstack-placementapi-tn1.cern.ch
        public:
          host: openstack-placementapi-tn1.cern.ch
      path:
        default: /
      scheme:
        default: 'http'
      port:
        api:
          default: 8778
          public: 80
    oslo_cache:
      hosts:
        default: "cci-mem-ay87ah.cern.ch,cci-mem-bk2r76.cern.ch,cci-mem-c8ywae.cern.ch,cci-mem-dtjwt8.cern.ch,cci-mem-e4i8n5.cern.ch,cci-mem-f2h79o.cern.ch"
      host_fqdn_override:
        default: "cci-mem-ay87ah.cern.ch,cci-mem-bk2r76.cern.ch,cci-mem-c8ywae.cern.ch,cci-mem-dtjwt8.cern.ch,cci-mem-e4i8n5.cern.ch,cci-mem-f2h79o.cern.ch"
    oslo_db:
      auth:
        admin:
          username: admin
        nova:
          username: nova_cell1
      path: /nova_cell1
    oslo_db_api:
      auth:
        admin:
          username: admin
        nova:
          username: nova_api
      path: /nova_api
    oslo_db_cell0:
      auth:
        admin:
          username: admin
        nova:
          username: nova_cell0
      path: /nova_cell0
    oslo_messaging:
      auth:
        admin:
          username: rabbit
        nova:
          username: rabbit
      path: /nova

  manifests:
    cron_job_cell_setup: false
    cron_job_service_cleaner: false
    daemonset_compute: true
    deployment_api_metadata: true
    deployment_api_osapi: true
    deployment_placement: true
    deployment_conductor: true
    deployment_consoleauth: false
    deployment_novncproxy: true
    deployment_spiceproxy: false
    deployment_scheduler: true
    ingress_api: true
    job_bootstrap: false
    job_cell_setup: false
    job_db_init: false
    job_db_init_placement: false
    job_db_sync: false
    job_image_repo_sync: false
    job_rabbit_init: false
    job_ks_endpoints: false
    job_ks_service: false
    job_ks_user: false
    job_ks_user_domain: false
    job_ks_placement_endpoints: false
    job_ks_placement_service: false
    job_ks_placement_user: false
    pdb_api: false
    pdb_metadata: false
    pdb_osapi: false
    pdb_placement: false
    pod_rally_test: false
    secret_rabbitmq: false

neutron:
  images:
    tags:
      neutron_server: docker.io/rochaporto/neutron:stein
      neutron_linuxbridge_agent: docker.io/rochaporto/neutron:stein
      dep_check: 'quay.io/stackanetes/kubernetes-entrypoint:v0.3.1'
    pull_policy: "IfNotPresent"
    local_registry:
      active: false
      exclude:
        - dep_check
        - image_repo_sync
  network:
    backend:
      - linuxbridge
    server:
      ingress:
        public: true
        classes:
          namespace: "tn"
          cluster: "nginx-cluster"
        annotations:
          kubernetes.io/ingress.class: traefik
          traefik.frontend.entryPoints: "https"
      external_policy_local: false
  dependencies:
    static:
      server:
        jobs: []
        services: []
      lb_agent:
        jobs: []
        services: []
  pod:
    lifecycle:
      upgrades:
        daemonsets:
          dhcp_agent:
            enabled: false
          l3_agent:
            enabled: false
          lb_agent:
            enabled: true
          metadata_agent:
            enabled: false
          ovs_agent:
            enabled: false
          sriov_agent:
            enabled: false
  conf:
    neutron:
      DEFAULT:
        api_extensions_path: /var/lib/openstack/lib/python3.6/site-packages/networking_cern/extensions
        service_plugins: cern
        l3_ha: "false"
        l3_ha_network_type: ""
        allow_automatic_l3agent_failover: "false"
        dhcp_agent_notification: "false"
        notify_nova_on_port_status_changes: "false"
        notify_nova_on_port_data_changes: "false"
      keystone_authtoken:
        auth_type: password
        auth_version: v3
      oslo_messaging_rabbit:
        rabbit_ha_queues: false
      CERN:
        landb_hostname: network.cern.ch
        landb_port: 443
        landb_protocol: https
        landb_username: vmmaster
        landb_manager: ai-openstack-admin
        landb_version: 6
    plugins:
      ml2_conf:
        ml2:
          #extension_drivers: ""
          mechanism_drivers: linuxbridge,cern_landb
          type_drivers: flat
          tenant_network_types: flat
        ml2_type_vxlan:
          vni_ranges: ""
          vxlan_group: ""
      linuxbridge_agent:
        linux_bridge:
          bridge_mappings: ""
          physical_interface_mappings: "external:eth2"
        securitygroup:
          firewall_driver: noop
        vxlan:
          enable_vxlan: False
          l2_population: False
          arp_responder: False
  endpoints:
    identity:
      name: keystone
      auth:
        neutron:
          role: admin
          username: svcneu
          project_name: services
          user_domain_name: Default
          project_domain_name: Default
      hosts:
        default: keystone.cern.ch
        internal: keystone.cern.ch
      host_fqdn_override:
        default: keystone.cern.ch
      path:
        default: /v3
      scheme:
        default: https
      port:
        api:
          default: 443
          internal: 443
    network:
      name: neutron
      hosts:
        default: neutron-server
        public: openstack-neutron-tn1
      host_fqdn_override:
        default: openstack-neutron-tn1.cern.ch
        public:
          host: openstack-neutron-tn1.cern.ch
      scheme:
        default: 'http'
      port:
        api:
          default: 9696
          public: 80
    oslo_cache:
      hosts:
        default: "cci-mem-ay87ah.cern.ch,cci-mem-bk2r76.cern.ch,cci-mem-c8ywae.cern.ch,cci-mem-dtjwt8.cern.ch,cci-mem-e4i8n5.cern.ch,cci-mem-f2h79o.cern.ch"
      host_fqdn_override:
        default: "cci-mem-ay87ah.cern.ch,cci-mem-bk2r76.cern.ch,cci-mem-c8ywae.cern.ch,cci-mem-dtjwt8.cern.ch,cci-mem-e4i8n5.cern.ch,cci-mem-f2h79o.cern.ch"
    oslo_db:
      auth:
        admin:
          username: admin
        neutron:
          username: neutron
    oslo_messaging:
      auth:
        admin:
          username: rabbit
        neutron:
          username: rabbit
      path: /neutron
  manifests:
    daemonset_dhcp_agent: false
    daemonset_l3_agent: false
    daemonset_lb_agent: true
    daemonset_metadata_agent: false
    daemonset_netns_cleanup_cron: false
    daemonset_ovs_agent: false
    daemonset_sriov_agent: false
    ingress_server: true
    job_bootstrap: false
    job_db_init: false
    job_db_sync: false
    job_image_repo_sync: false
    job_ks_endpoints: false
    job_ks_service: false
    job_ks_user: false
    job_rabbit_init: false
    pdb_server: false
    pod_rally_test: false
    secret_rabbitmq: false

rabbitmq-ha:
  definitions:
    permissions: |-
      {
        "user": "rabbit",
        "vhost": "glance",
        "configure": ".*",
        "write": ".*",
        "read": ".*"
      },
      {
        "user": "rabbit",
        "vhost": "heat",
        "configure": ".*",
        "write": ".*",
        "read": ".*"
      },
      {
        "user": "rabbit",
        "vhost": "magnum",
        "configure": ".*",
        "write": ".*",
        "read": ".*"
      },
      {
        "user": "rabbit",
        "vhost": "neutron",
        "configure": ".*",
        "write": ".*",
        "read": ".*"
      },
      {
        "user": "rabbit",
        "vhost": "nova",
        "configure": ".*",
        "write": ".*",
        "read": ".*"
      }
    vhosts: |-
      {
        "name": "glance"
      },
      {
        "name": "heat"
      },
      {
        "name": "magnum"
      },
      {
        "name": "neutron"
      },
      {
        "name": "nova"
      }
  image:
    tag: 3.8.0-alpine
  rabbitmqMemoryHighWatermark: 0.9
  rabbitmqMemoryHighWatermarkType: relative
  resources:
    limits:
      cpu: 4000m
      memory: 4Gi
    requests:
      cpu: 2000m
      memory: 2Gi
  managementUsername: mgmt
  persistentVolume:
    enabled: false
  rabbitmqUsername: rabbit
  service:
    type: NodePort
    epmdNodePort: 32671
    amqpNodePort: 32672
    managerNodePort: 32673
