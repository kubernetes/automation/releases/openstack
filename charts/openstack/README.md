# Use the control plane cluster
```bash
export OS_PROJECT_NAME="Cloud Infra Services"
#| 723d57fb-a637-4c6c-a2d6-c29be43988e0 | cci-tn1-control-plane-001  | strigazi-lxplus |          3 |            1 | CREATE_COMPLETE | None          |
openstack coe cluster config cci-tn1-control-plane-001 --dir ~/.kube --force
```
Install openebs:
```bash
kubectl create ns openebs
helm repo add openebs https://openebs.github.io/charts
helm install openebs openebs/openebs
kubectl apply -f openebs-hostpath-1.yaml
```
Get the secrets (could be done with the plugin, didn't try yet):
```bash
tbag show --hg cloud_adm strigazi-tn-secrets  --file tn-secrets.yaml
```

Install control-plane on the control plane cluster:
```bash
kubectl create ns tn
helm dep update
helm install --namespace tn tn . -f values.yaml -f tn-values.yaml -f tn-secrets.yaml
```

# Use the compute nodes cluster

Install compute nodes on the compute node cluster:
```bash
#| a78e4efd-66ef-4159-9000-159fdfce08a1 | cci-compute-nodes-001      | strigazi-lxplus |          3 |            1 | CREATE_COMPLETE | None          |
openstack coe cluster config cci-compute-nodes-001 --dir ~/.kube --force
kubectl create ns tn
helm install --namespace tn openstack-tn . -f values.yaml -f tn-compute-values.yaml -f tn-secrets.yaml
```
