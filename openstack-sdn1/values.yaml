neutron:
  images:
    tags:
      neutron_server: docker.io/openstackhelm/neutron:newton-ubuntu_xenial
      dep_check: quay.io/stackanetes/kubernetes-entrypoint:v0.3.0
      opencontrail_neutron_init: docker.io/opencontrailnightly/contrail-openstack-neutron-init:5.1.0-latest-newton
    pull_policy: "IfNotPresent"
    local_registry:
      active: false
      #exclude:
      #  - dep_check
      #  - image_repo_sync
  
  endpoints:
    cluster_domain_suffix: cluster.local
    oslo_cache:
      auth:
        memcache_secret_key: null
      hosts:
        default: null
      host_fqdn_override:
        default: null
      port:
        memcache:
          default: 11211
    identity:
      name: keystone
      auth:
        admin:
          region_name: sdn1
          username: svcneu
          project_name: services
          user_domain_name:
          project_domain_name:
        neutron:
          role: service
          region_name: sdn1
          username: svcneu
          project_name: services
          user_domain_name: Default
          project_domain_name: Default
      hosts:
        default: keystone.cern.ch
        public: keystone.cern.ch
      host_fqdn_override:
        default: keystone.cern.ch
      path:
        default: /v3
      scheme:
        default: https
      port:
        admin:
          default: 443
        api:
          default: 443
    network:
      name: neutron
      hosts:
        default: neutron-server
        public: openstacksdn.cern.ch
      host_fqdn_override:
        default: null
      path:
        default: null
      scheme:
        default: 'https'
      port:
        api:
          default: 9696
          public: 9696
  
  network:
    backend:
      - opencontrail
    server:
      ingress:
        public: true
        classes:
          namespace: "kube-system"
          cluster: "nginx-cluster"
        annotations:
          kubernetes.io/ingress.class: traefik
          traefik.ingress.kubernetes.io/frontend-entry-points: http
      external_policy_local: false
      node_port:
        enabled: false
        port: 30096
  
  conf:
    neutron:
      DEFAULT:
        debug: True
        verbose: True
        core_plugin: neutron_plugin_contrail.plugins.opencontrail.contrail_plugin.NeutronPluginContrailCoreV2
        service_plugins:
          - neutron_plugin_contrail.plugins.opencontrail.loadbalancer.v2.plugin.LoadBalancerPluginV2
        l3_ha: False
        api_extensions_path: /opt/plugin/site-packages/neutron_plugin_contrail/extensions:/opt/plugin/site-packages/neutron_lbaas/extensions
        interface_driver: null
      quotas:
        quota_driver: neutron_plugin_contrail.plugins.opencontrail.quota.driver.QuotaDriver
        default_quota: -1
        quota_network: 5
        quota_loadbalancer: 0
        quota_floatingip: 0
      keystone_authtoken:
        memcached_servers: null
        memcache_security_strategy: None
        auth_type: password
        auth_version: v3
      oslo_messaging_notifications:
        driver: noop
    paste:
      composite:neutronapi_v2_0:
        keystone: user_token cors http_proxy_to_wsgi request_id catch_errors authtoken keystonecontext extensions neutronapiapp_v2_0
      filter:user_token:
        paste.filter_factory: neutron_plugin_contrail.plugins.opencontrail.neutron_middleware:token_factory
    policy:
      context_is_admin: role:admin
      owner: tenant_id:%(tenant_id)s
      admin_or_owner: rule:context_is_admin or rule:owner
      context_is_advsvc: role:advsvc
      admin_or_network_owner: rule:context_is_admin or tenant_id:%(network:tenant_id)s
      admin_owner_or_network_owner: rule:owner or rule:admin_or_network_owner
      admin_only: rule:context_is_admin
      regular_user: ''
      shared: field:networks:shared=True
      shared_subnetpools: field:subnetpools:shared=True
      shared_address_scopes: field:address_scopes:shared=True
      external: field:networks:router:external=True
      default: rule:admin_or_owner
      create_subnet: rule:admin_or_network_owner
      create_subnet:segment_id: rule:admin_only
      create_subnet:service_types: rule:admin_only
      get_subnet: rule:admin_or_owner or rule:shared
      get_subnet:segment_id: rule:admin_only
      update_subnet: rule:admin_or_network_owner
      update_subnet:service_types: rule:admin_only
      delete_subnet: rule:admin_or_network_owner
      create_subnetpool: ''
      create_subnetpool:shared: rule:admin_only
      create_subnetpool:is_default: rule:admin_only
      get_subnetpool: rule:admin_or_owner or rule:shared_subnetpools
      update_subnetpool: rule:admin_or_owner
      update_subnetpool:is_default: rule:admin_only
      delete_subnetpool: rule:admin_or_owner
      create_address_scope: ''
      create_address_scope:shared: rule:admin_only
      get_address_scope: rule:admin_or_owner or rule:shared_address_scopes
      update_address_scope: rule:admin_or_owner
      update_address_scope:shared: rule:admin_only
      delete_address_scope: rule:admin_or_owner
      get_network: rule:admin_or_owner or rule:shared or rule:external or rule:context_is_advsvc
      get_network:router:external: rule:regular_user
      get_network:segments: rule:admin_only
      get_network:provider:network_type: rule:admin_only
      get_network:provider:physical_network: rule:admin_only
      get_network:provider:segmentation_id: rule:admin_only
      get_network:queue_id: rule:admin_only
      get_network_ip_availabilities: rule:admin_only
      get_network_ip_availability: rule:admin_only
      create_network: rule:regular_user
      create_network:shared: rule:admin_or_owner
      create_network:router:external: rule:admin_or_owner
      create_network:is_default: rule:admin_or_owner
      create_network:segments: rule:admin_or_owner
      create_network:provider:network_type: rule:admin_or_owner
      create_network:provider:physical_network: rule:admin_or_owner
      create_network:provider:segmentation_id: rule:admin_only
      update_network: rule:admin_or_owner
      update_network:segments: rule:admin_only
      update_network:shared: rule:admin_only
      update_network:provider:network_type: rule:admin_only
      update_network:provider:physical_network: rule:admin_only
      update_network:provider:segmentation_id: rule:admin_only
      update_network:router:external: rule:admin_only
      delete_network: rule:admin_or_owner
      create_segment: rule:admin_only
      get_segment: rule:admin_only
      update_segment: rule:admin_only
      delete_segment: rule:admin_only
      network_device: 'field:port:device_owner=~^network:'
      create_port: ''
      create_port:device_owner: not rule:network_device or rule:context_is_advsvc or rule:admin_or_network_owner
      create_port:mac_address: rule:context_is_advsvc or rule:admin_or_network_owner
      create_port:fixed_ips: rule:context_is_advsvc or rule:admin_or_network_owner
      create_port:port_security_enabled: rule:context_is_advsvc or rule:admin_or_network_owner
      create_port:binding:host_id: rule:admin_only
      create_port:binding:profile: rule:admin_only
      create_port:mac_learning_enabled: rule:context_is_advsvc or rule:admin_or_network_owner
      create_port:allowed_address_pairs: rule:admin_or_network_owner
      get_port: rule:context_is_advsvc or rule:admin_owner_or_network_owner
      get_port:queue_id: rule:admin_only
      get_port:binding:vif_type: rule:admin_only
      get_port:binding:vif_details: rule:admin_only
      get_port:binding:host_id: rule:admin_only
      get_port:binding:profile: rule:admin_only
      update_port: rule:admin_or_owner or rule:context_is_advsvc
      update_port:device_owner: not rule:network_device or rule:context_is_advsvc or rule:admin_or_network_owner
      update_port:mac_address: rule:admin_only or rule:context_is_advsvc
      update_port:fixed_ips: rule:context_is_advsvc or rule:admin_or_network_owner
      update_port:port_security_enabled: rule:context_is_advsvc or rule:admin_or_network_owner
      update_port:binding:host_id: rule:admin_only
      update_port:binding:profile: rule:admin_only
      update_port:mac_learning_enabled: rule:context_is_advsvc or rule:admin_or_network_owner
      update_port:allowed_address_pairs: rule:admin_or_network_owner
      delete_port: rule:context_is_advsvc or rule:admin_owner_or_network_owner
      get_router:ha: rule:admin_only
      create_router: rule:regular_user
      create_router:external_gateway_info:enable_snat: rule:admin_only
      create_router:distributed: rule:admin_only
      create_router:ha: rule:admin_only
      get_router: rule:admin_or_owner
      get_router:distributed: rule:admin_only
      update_router:external_gateway_info:enable_snat: rule:admin_only
      update_router:distributed: rule:admin_only
      update_router:ha: rule:admin_only
      delete_router: rule:admin_or_owner
      add_router_interface: rule:admin_or_owner
      remove_router_interface: rule:admin_or_owner
      create_router:external_gateway_info:external_fixed_ips: rule:admin_only
      update_router:external_gateway_info:external_fixed_ips: rule:admin_only
      insert_rule: rule:admin_or_owner
      remove_rule: rule:admin_or_owner
      create_qos_queue: rule:admin_only
      get_qos_queue: rule:admin_only
      update_agent: rule:admin_only
      delete_agent: rule:admin_only
      get_agent: rule:admin_only
      create_dhcp-network: rule:admin_only
      delete_dhcp-network: rule:admin_only
      get_dhcp-networks: rule:admin_only
      create_l3-router: rule:admin_only
      delete_l3-router: rule:admin_only
      get_l3-routers: rule:admin_only
      get_dhcp-agents: rule:admin_only
      get_l3-agents: rule:admin_only
      get_loadbalancer-agent: rule:admin_only
      get_loadbalancer-pools: rule:admin_only
      get_agent-loadbalancers: rule:admin_only
      get_loadbalancer-hosting-agent: rule:admin_only
      create_floatingip: rule:regular_user
      create_floatingip:floating_ip_address: rule:admin_only
      update_floatingip: rule:admin_or_owner
      delete_floatingip: rule:admin_or_owner
      get_floatingip: rule:admin_or_owner or rule:context_is_advsvc
      create_network_profile: rule:admin_only
      update_network_profile: rule:admin_only
      delete_network_profile: rule:admin_only
      get_network_profiles: ''
      get_network_profile: ''
      update_policy_profiles: rule:admin_only
      get_policy_profiles: ''
      get_policy_profile: ''
      create_metering_label: rule:admin_only
      delete_metering_label: rule:admin_only
      get_metering_label: rule:admin_only
      create_metering_label_rule: rule:admin_only
      delete_metering_label_rule: rule:admin_only
      get_metering_label_rule: rule:admin_only
      get_service_provider: rule:regular_user
      get_lsn: rule:admin_only
      create_lsn: rule:admin_only
      create_flavor: rule:admin_only
      update_flavor: rule:admin_only
      delete_flavor: rule:admin_only
      get_flavors: rule:regular_user
      get_flavor: rule:regular_user
      create_service_profile: rule:admin_only
      update_service_profile: rule:admin_only
      delete_service_profile: rule:admin_only
      get_service_profiles: rule:admin_only
      get_service_profile: rule:admin_only
      get_policy: rule:regular_user
      create_policy: rule:admin_only
      update_policy: rule:admin_only
      delete_policy: rule:admin_only
      get_policy_bandwidth_limit_rule: rule:regular_user
      create_policy_bandwidth_limit_rule: rule:admin_only
      delete_policy_bandwidth_limit_rule: rule:admin_only
      update_policy_bandwidth_limit_rule: rule:admin_only
      get_policy_dscp_marking_rule: rule:regular_user
      create_policy_dscp_marking_rule: rule:admin_only
      delete_policy_dscp_marking_rule: rule:admin_only
      update_policy_dscp_marking_rule: rule:admin_only
      get_rule_type: rule:regular_user
      get_policy_minimum_bandwidth_rule: rule:regular_user
      create_policy_minimum_bandwidth_rule: rule:admin_only
      delete_policy_minimum_bandwidth_rule: rule:admin_only
      update_policy_minimum_bandwidth_rule: rule:admin_only
      restrict_wildcard: "(not field:rbac_policy:target_tenant=*) or rule:admin_only"
      create_rbac_policy: ''
      create_rbac_policy:target_tenant: rule:restrict_wildcard
      update_rbac_policy: rule:admin_or_owner
      update_rbac_policy:target_tenant: rule:restrict_wildcard and rule:admin_or_owner
      get_rbac_policy: rule:admin_or_owner
      delete_rbac_policy: rule:admin_or_owner
      create_flavor_service_profile: rule:admin_only
      delete_flavor_service_profile: rule:admin_only
      get_flavor_service_profile: rule:regular_user
      get_auto_allocated_topology: rule:admin_or_owner
      create_trunk: rule:regular_user
      get_trunk: rule:admin_or_owner
      delete_trunk: rule:admin_or_owner
      get_subports: ''
      add_subports: rule:admin_or_owner
      remove_subports: rule:admin_or_owner
    plugins:
      opencontrail:
        APISERVER:
          api_server_ip: config-api-sdn.cern.ch
          api_server_port: 8082
          #contrail_extensions: "ipam:neutron_plugin_contrail.plugins.opencontrail.contrail_plugin_ipam.NeutronPluginContrailIpam,policy:neutron_plugin_contrail.plugins.opencontrail.contrail_plugin_policy.NeutronPluginContrailPolicy,route-table:neutron_plugin_contrail.plugins.opencontrail.contrail_plugin_vpc.NeutronPluginContrailVpc,contrail:None,service-interface:None,vf-binding:None"
          contrail_extensions: "ipam:neutron_plugin_contrail.plugins.opencontrail.contrail_plugin_ipam.NeutronPluginContrailIpam,policy:neutron_plugin_contrail.plugins.opencontrail.contrail_plugin_policy.NeutronPluginContrailPolicy,contrail:None,service-interface:None,vf-binding:None,security-group:None"
          multi_tenancy: True
          auth_token_url: https://keystone.cern.ch:443/v3/auth/tokens
        COLLECTOR:
          analytics_api_ip: analytics-api-sdn.cern.ch
          analytics_api_port: 8081
  
  dependencies:
    dynamic:
    targeted:
      openvswitch:
        dhcp:
          pod:
            - labels:
                application: neutron
                component: neutron-ovs-agent
        l3:
          pod:
            - labels:
                application: neutron
                component: neutron-ovs-agent
        metadata:
          pod:
            - labels:
                application: neutron
                component: neutron-ovs-agent
      linuxbridge:
        dhcp:
          pod:
            - labels:
                application: neutron
                component: neutron-lb-agent
        l3:
          pod:
            - labels:
                application: neutron
                component: neutron-lb-agent
        metadata:
          pod:
            - labels:
                application: neutron
                component: neutron-lb-agent
        lb_agent:
          pod: null
      sriov:
        dhcp:
          pod:
            - labels:
                application: neutron
                component: neutron-sriov-agent
        l3:
          pod:
            - labels:
                application: neutron
                component: neutron-sriov-agent
        metadata:
          pod:
            - labels:
                application: neutron
                component: neutron-sriov-agent
  static:
    bootstrap:
      services:
        - endpoint: internal
          service: network
        - endpoint: internal
          service: compute
    db_drop:
      services:
        - endpoint: internal
          service: oslo_db
    db_init:
      services:
        - endpoint: internal
          service: oslo_db
    db_sync:
      jobs:
        - neutron-db-init
      services:
        - endpoint: internal
          service: oslo_db
    dhcp:
      pod: null
      jobs:
        - neutron-rabbit-init
      services:
        - endpoint: internal
          service: oslo_messaging
        - endpoint: internal
          service: network
        - endpoint: internal
          service: compute
    ks_endpoints:
      jobs:
        - neutron-ks-service
      services:
        - endpoint: internal
          service: identity
    ks_service:
      services:
        - endpoint: internal
          service: identity
    ks_user:
      services:
        - endpoint: internal
          service: identity
    rabbit_init:
      services:
        - service: oslo_messaging
          endpoint: internal
    l3:
      pod: null
      jobs:
        - neutron-rabbit-init
      services:
        - endpoint: internal
          service: oslo_messaging
        - endpoint: internal
          service: network
        - endpoint: internal
          service: compute
    lb_agent:
      pod: null
      jobs:
        - neutron-rabbit-init
      services:
        - endpoint: internal
          service: oslo_messaging
        - endpoint: internal
          service: network
    metadata:
      pod: null
      jobs:
        - neutron-rabbit-init
      services:
        - endpoint: internal
          service: oslo_messaging
        - endpoint: internal
          service: network
        - endpoint: internal
          service: compute
        - endpoint: public
          service: compute_metadata
    ovs_agent:
      jobs:
        - neutron-rabbit-init
      pod:
        - labels:
            application: openvswitch
            component: openvswitch-vswitchd
        - labels:
            application: openvswitch
            component: openvswitch-vswitchd-db
      services:
        - endpoint: internal
          service: oslo_messaging
        - endpoint: internal
          service: network
    server:
      jobs:
        - neutron-db-sync
        - neutron-ks-user
        - neutron-ks-endpoints
        - neutron-rabbit-init
      services:
        - endpoint: internal
          service: oslo_db
        - endpoint: internal
          service: oslo_messaging
        - endpoint: internal
          service: oslo_cache
        - endpoint: internal
          service: identity
    tests:
      services:
        - endpoint: internal
          service: network
        - endpoint: internal
          service: compute
  dependencies:
    dynamic:
      targeted:
        opencontrail:
          server:
            daemonset: []
    static:
      server:
        jobs: []
        services: []
  
  manifests:
    configmap_bin: true
    configmap_etc: true
    daemonset_dhcp_agent: false
    daemonset_l3_agent: false
    daemonset_lb_agent: false
    daemonset_metadata_agent: false
    daemonset_ovs_agent: false
    daemonset_sriov_agent: false
    deployment_server: true
    ingress_server: true
    job_bootstrap: false
    job_db_init: false
    job_db_sync: false
    job_db_drop: false
    job_ks_endpoints: false
    job_ks_service: false
    job_ks_user: false
    job_rabbit_init: false
    pdb_server: false
    pod_rally_test: false
    secret_db: true
    secret_keystone: false
    secret_rabbitmq: false
    service_ingress_server: false
    service_server: true

horizon:
  images:
    tags:
      horizon: docker.io/openstackhelm/horizon:rocky-ubuntu_xenial-20190603 
      dep_check: quay.io/stackanetes/kubernetes-entrypoint:v0.3.0
    pull_policy: "IfNotPresent"
  
  network:
    dashboard:
      ingress:
        public: true
        classes:
          namespace: "traefik"
          cluster: "nginx-cluster"
        annotations:
          kubernetes.io/ingress.class: traefik
          traefik.frontend.entryPoints: "https"
    external_policy_local: false
    node_port:
      enabled: false
      port: 31000
  
  conf:
    horizon:
      apache: |
        Listen 0.0.0.0:{{ tuple "dashboard" "internal" "web" . | include "helm-toolkit.endpoints.endpoint_port_lookup" }}
        ServerName "{{ .Values.endpoints.dashboard.host_fqdn_override.default }}"
  
        LogFormat "%h %l %u %t \"%r\" %>s %b \"%{Referer}i\" \"%{User-Agent}i\"" combined
        LogFormat "%{X-Forwarded-For}i %l %u %t \"%r\" %>s %b \"%{Referer}i\" \"%{User-Agent}i\"" proxy
  
        SetEnvIf X-Forwarded-For "^.*\..*\..*\..*" forwarded
        CustomLog /dev/stdout combined env=!forwarded
        CustomLog /dev/stdout proxy env=forwarded
  
        <VirtualHost *:{{ tuple "dashboard" "internal" "web" . | include "helm-toolkit.endpoints.endpoint_port_lookup" }}>
            WSGIScriptReloading On
            WSGIDaemonProcess horizon-http processes=5 threads=1 user=horizon group=horizon display-name=%{GROUP} python-path=/var/lib/kolla/venv/lib/python2.7/site-packages
            WSGIProcessGroup horizon-http
            WSGIScriptAlias / /var/www/cgi-bin/horizon/django.wsgi
            WSGIPassAuthorization On
  
            <Location "/">
                Require all granted
            </Location>
  
            Alias /static /var/www/html/horizon
            <Location "/static">
                SetHandler None
            </Location>
  
            <IfVersion >= 2.4>
              ErrorLogFormat "%{cu}t %M"
            </IfVersion>
            ErrorLog /dev/stdout
            TransferLog /dev/stdout
  
            SetEnvIf X-Forwarded-For "^.*\..*\..*\..*" forwarded
            CustomLog /dev/stdout combined env=!forwarded
            CustomLog /dev/stdout proxy env=forwarded
        </Virtualhost>
      local_settings:
        config:
          horizon_secret_key: 9aee62c0-5253-4a86-b189-e0fb71fa503c
          debug: "True"
          keystone_multidomain_support: "False"
          keystone_default_domain: "default"
          openstack_cinder_features:
            enable_backup: "False"
          openstack_neutron_network:
            enable_router: "True"
            enable_quotas: "True"
            enable_ipv6: "True"
            enable_distributed_router: "False"
            enable_ha_router: "False"
            enable_lb: "True"
            enable_firewall: "True"
            enable_vpn: "True"
            enable_fip_topology_check: "True"
          auth:
            sso:
              enabled: True
              initial_choice: "saml2"
            idp_mapping:
              - name: "saml2"
                label: "CERN SSO"
                idp: "cern"
                protocol: "saml2"
        template: |
          import os
  
          from django.utils.translation import ugettext_lazy as _
  
          from openstack_dashboard import exceptions
  
          DEBUG = {{ .Values.conf.horizon.local_settings.config.debug }}
          TEMPLATE_DEBUG = DEBUG
  
          COMPRESS_OFFLINE = True
          COMPRESS_CSS_HASHING_METHOD = "hash"
  
          WEBROOT = '/'
  
          ALLOWED_HOSTS = ['*']
  
          SECURE_PROXY_SSL_HEADER = ('HTTP_X_FORWARDED_PROTO', 'https')
  
          CSRF_COOKIE_SECURE = True
          SESSION_COOKIE_SECURE = True
  
          OPENSTACK_API_VERSIONS = {
              "data-processing": 1.1,
              "identity": 3,
              "volume": 2,
          }
  
          OPENSTACK_KEYSTONE_DEFAULT_DOMAIN = '{{ .Values.conf.horizon.local_settings.config.keystone_default_domain }}'
  
          HORIZON_CONFIG = {
              'user_home': 'openstack_dashboard.views.get_user_home',
              'ajax_queue_limit': 10,
              'auto_fade_alerts': {
                  'delay': 3000,
                  'fade_duration': 1500,
                  'types': ['alert-success', 'alert-info']
              },
              'help_url': "https://clouddocs.web.cern.ch/clouddocs",
              'exceptions': {'recoverable': exceptions.RECOVERABLE,
                             'not_found': exceptions.NOT_FOUND,
                             'unauthorized': exceptions.UNAUTHORIZED},
              'modal_backdrop': 'static',
              'angular_modules': [],
              'js_files': [],
              'js_spec_files': [],
          }
  
          LOCAL_PATH = os.path.dirname(os.path.abspath(__file__))
  
          SECRET_KEY='{{ .Values.conf.horizon.local_settings.config.horizon_secret_key }}'
  
          CACHES = {
              'default': {
                  'BACKEND': 'django.core.cache.backends.memcached.MemcachedCache',
                  'LOCATION': '{{ tuple "oslo_cache" "internal" "memcache" . | include "helm-toolkit.endpoints.host_and_port_endpoint_uri_lookup" }}',
              }
          }
          SESSION_ENGINE = 'django.contrib.sessions.backends.cache'
  
          EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'
  
          OPENSTACK_KEYSTONE_URL = "{{ tuple "identity" "public" "api" . | include "helm-toolkit.endpoints.keystone_endpoint_uri_lookup" }}"
          OPENSTACK_KEYSTONE_DEFAULT_ROLE = "Member"
  
  
          {{- if .Values.conf.horizon.local_settings.config.auth.sso.enabled }}
          WEBSSO_ENABLED = True
  
          WEBSSO_INITIAL_CHOICE = "{{ .Values.conf.horizon.local_settings.config.auth.sso.initial_choice }}"
  
          WEBSSO_CHOICES = (
              ("credentials", _("Keystone Credentials")),
            {{- range $i, $sso := .Values.conf.horizon.local_settings.config.auth.idp_mapping }}
              ({{ $sso.name | quote }}, {{ $sso.label | quote }}),
            {{- end }}
          )
  
          WEBSSO_IDP_MAPPING = {
            {{- range $i, $sso := .Values.conf.horizon.local_settings.config.auth.idp_mapping }}
              {{ $sso.name | quote}}: ({{ $sso.idp | quote }}, {{ $sso.protocol | quote }}),
            {{- end }}
          }
  
          {{- end }}
  
          OPENSTACK_SSL_NO_VERIFY = True
  
          OPENSTACK_KEYSTONE_BACKEND = {
              'name': 'native',
              'can_edit_user': False,
              'can_edit_group': False,
              'can_edit_project': False,
              'can_edit_domain': False,
              'can_edit_role': False,
          }
  
          LAUNCH_INSTANCE_DEFAULTS = {
            'config_driver': False,
            'enable_scheduler_hints': False,
            'disable_image': False,
            'disable_instance_snapshot': False,
            'disable_volume': False,
            'disable_volume_snapshot': False,
            'create_volume': False,
          }
  
          OPENSTACK_HYPERVISOR_FEATURES = {
              'can_set_mount_point': True,
              'can_set_password': False,
          }
  
          OPENSTACK_CINDER_FEATURES = {
              'enable_backup': {{ .Values.conf.horizon.local_settings.config.openstack_cinder_features.enable_backup }},
          }
  
          OPENSTACK_NEUTRON_NETWORK = {
              'enable_router': {{ .Values.conf.horizon.local_settings.config.openstack_neutron_network.enable_router }},
              'enable_quotas': {{ .Values.conf.horizon.local_settings.config.openstack_neutron_network.enable_quotas }},
              'enable_ipv6': {{ .Values.conf.horizon.local_settings.config.openstack_neutron_network.enable_ipv6 }},
              'enable_distributed_router': {{ .Values.conf.horizon.local_settings.config.openstack_neutron_network.enable_distributed_router }},
              'enable_ha_router': {{ .Values.conf.horizon.local_settings.config.openstack_neutron_network.enable_ha_router }},
              'enable_lb': {{ .Values.conf.horizon.local_settings.config.openstack_neutron_network.enable_lb }},
              'enable_firewall': {{ .Values.conf.horizon.local_settings.config.openstack_neutron_network.enable_firewall }},
              'enable_vpn': {{ .Values.conf.horizon.local_settings.config.openstack_neutron_network.enable_vpn }},
              'enable_fip_topology_check': {{ .Values.conf.horizon.local_settings.config.openstack_neutron_network.enable_fip_topology_check }},
              'profile_support': None,
              'supported_provider_types': ['*'],
              'supported_vnic_types': ['*']
          }
  
          API_RESULT_LIMIT = 1000
          API_RESULT_PAGE_SIZE = 20
  
          DROPDOWN_MAX_ITEMS = 400
  
          TIME_ZONE = "Europe/Zurich"
  
          POLICY_FILES_PATH = '/etc/openstack-dashboard'
          LOGGING = {
              'version': 1,
              'disable_existing_loggers': False,
              'handlers': {
                  'console': {
                      'level': 'INFO',
                      'class': 'logging.StreamHandler',
                  },
              },
              'loggers': {
                  'django.db.backends': {
                      'handlers': ['console'],
                      'propagate': False,
                  },
                  'requests': {
                      'handlers': ['console'],
                      'propagate': False,
                  },
                  'horizon': {
                      'handlers': ['console'],
                      'level': 'DEBUG',
                      'propagate': False,
                  },
                  'openstack_dashboard': {
                      'handlers': ['console'],
                      'level': 'DEBUG',
                      'propagate': False,
                  },
                  'novaclient': {
                      'handlers': ['console'],
                      'level': 'DEBUG',
                      'propagate': False,
                  },
                  'cinderclient': {
                      'handlers': ['console'],
                      'level': 'DEBUG',
                      'propagate': False,
                  },
                  'glanceclient': {
                      'handlers': ['console'],
                      'level': 'DEBUG',
                      'propagate': False,
                  },
                  'glanceclient': {
                      'handlers': ['console'],
                      'level': 'DEBUG',
                      'propagate': False,
                  },
                  'neutronclient': {
                      'handlers': ['console'],
                      'level': 'DEBUG',
                      'propagate': False,
                  },
                  'heatclient': {
                      'handlers': ['console'],
                      'level': 'DEBUG',
                      'propagate': False,
                  },
                  'ceilometerclient': {
                      'handlers': ['console'],
                      'level': 'DEBUG',
                      'propagate': False,
                  },
                  'troveclient': {
                      'handlers': ['console'],
                      'level': 'DEBUG',
                      'propagate': False,
                  },
                  'swiftclient': {
                      'handlers': ['console'],
                      'level': 'DEBUG',
                      'propagate': False,
                  },
                  'openstack_auth': {
                      'handlers': ['console'],
                      'level': 'DEBUG',
                      'propagate': False,
                  },
                  'nose.plugins.manager': {
                      'handlers': ['console'],
                      'level': 'DEBUG',
                      'propagate': False,
                  },
                  'django': {
                      'handlers': ['console'],
                      'level': 'DEBUG',
                      'propagate': False,
                  },
                  'iso8601': {
                      'handlers': ['console'],
                      'propagate': False,
                  },
                  'scss': {
                      'handlers': ['console'],
                      'propagate': False,
                  },
              }
          }
  
          SECURITY_GROUP_RULES = {
              'all_tcp': {
                  'name': _('All TCP'),
                  'ip_protocol': 'tcp',
                  'from_port': '1',
                  'to_port': '65535',
              },
              'all_udp': {
                  'name': _('All UDP'),
                  'ip_protocol': 'udp',
                  'from_port': '1',
                  'to_port': '65535',
              },
              'all_icmp': {
                  'name': _('All ICMP'),
                  'ip_protocol': 'icmp',
                  'from_port': '-1',
                  'to_port': '-1',
              },
              'ssh': {
                  'name': 'SSH',
                  'ip_protocol': 'tcp',
                  'from_port': '22',
                  'to_port': '22',
              },
              'smtp': {
                  'name': 'SMTP',
                  'ip_protocol': 'tcp',
                  'from_port': '25',
                  'to_port': '25',
              },
              'dns': {
                  'name': 'DNS',
                  'ip_protocol': 'tcp',
                  'from_port': '53',
                  'to_port': '53',
              },
              'http': {
                  'name': 'HTTP',
                  'ip_protocol': 'tcp',
                  'from_port': '80',
                  'to_port': '80',
              },
              'pop3': {
                  'name': 'POP3',
                  'ip_protocol': 'tcp',
                  'from_port': '110',
                  'to_port': '110',
              },
              'imap': {
                  'name': 'IMAP',
                  'ip_protocol': 'tcp',
                  'from_port': '143',
                  'to_port': '143',
              },
              'ldap': {
                  'name': 'LDAP',
                  'ip_protocol': 'tcp',
                  'from_port': '389',
                  'to_port': '389',
              },
              'https': {
                  'name': 'HTTPS',
                  'ip_protocol': 'tcp',
                  'from_port': '443',
                  'to_port': '443',
              },
              'smtps': {
                  'name': 'SMTPS',
                  'ip_protocol': 'tcp',
                  'from_port': '465',
                  'to_port': '465',
              },
              'imaps': {
                  'name': 'IMAPS',
                  'ip_protocol': 'tcp',
                  'from_port': '993',
                  'to_port': '993',
              },
              'pop3s': {
                  'name': 'POP3S',
                  'ip_protocol': 'tcp',
                  'from_port': '995',
                  'to_port': '995',
              },
              'ms_sql': {
                  'name': 'MS SQL',
                  'ip_protocol': 'tcp',
                  'from_port': '1433',
                  'to_port': '1433',
              },
              'mysql': {
                  'name': 'MYSQL',
                  'ip_protocol': 'tcp',
                  'from_port': '3306',
                  'to_port': '3306',
              },
              'rdp': {
                  'name': 'RDP',
                  'ip_protocol': 'tcp',
                  'from_port': '3389',
                  'to_port': '3389',
              },
          }
          REST_API_REQUIRED_SETTINGS = ['OPENSTACK_HYPERVISOR_FEATURES',
                                        'LAUNCH_INSTANCE_DEFAULTS',
                                        'OPENSTACK_IMAGE_FORMATS']
          REST_API_ADDITIONAL_SETTINGS = ['VOLUME_API_META']
          STATIC_ROOT = '/var/www/html/horizon'
  
  dependencies:
    static:
      dashboard:
        jobs: []
        services: []
  
  pod:
    user:
      neutron:
        uid: 0
    affinity:
      anti:
        type:
          default: preferredDuringSchedulingIgnoredDuringExecution
        topologyKey:
          default: kubernetes.io/hostname
    mounts: {}
    replicas:
      server: 1
    lifecycle:
      upgrades:
        deployments:
          revision_history: 3
          pod_replacement_strategy: RollingUpdate
          rolling_update:
            max_unavailable: 1
            max_surge: 3
      disruption_budget:
        horizon:
          min_available: 0
      termination_grace_period:
        horizon:
          timeout: 30
    resources:
      enabled: false
      server:
        requests:
          memory: "128Mi"
          cpu: "100m"
        limits:
          memory: "1204Mi"
          cpu: "2000m"
  
  endpoints:
    cluster_domain_suffix: cluster.local
    identity:
      name: keystone
      hosts:
        default: keystone.cern.ch
        public: keystone.cern.ch
      host_fqdn_override:
        default: keystone.cern.ch
      path:
        default: /v3
      scheme:
        default: https
      port:
        admin:
          default: 443
        api:
          default: 443
    oslo_cache:
      hosts:
        default: memcached
      host_fqdn_override:
        default: null
      port:
        memcache:
          default: 11211
    dashboard:
      name: horizon
      hosts:
        default: horizon-int
        public: horizon
      host_fqdn_override:
        default: openstack-horizon-sdn1.cern.ch
      path:
        default: null
      scheme:
        default: http
      port:
        web:
          default: 80
    oslo_db:
      auth:
        admin:
          username: root
          password: password
        horizon:
          username: horizon
          password: password
      hosts:
        default: mariadb
      host_fqdn_override:
        default: null
      path: /horizon
      scheme: mysql+pymysql
      port:
        mysql:
          default: 3306
  
  manifests:
    configmap_bin: true
    configmap_etc: true
    deployment: true
    ingress_api: true
    #job_bootstrap: true
    job_db_init: false
    job_db_sync: false
    job_db_drop: false
    job_image_repo_sync: false
    job_ks_endpoints: false
    job_ks_service: false
    job_ks_user: false
    job_rabbit_init: false
    pdb: false
    secret_db: false
    service_ingress: true
    service: true


mariadb:
  volume:
    enabled: false

memcached:
  manifests:
  configmap_bin: true
  deployment: true
  job_image_repo_sync: true
  service: true
  monitoring:
    prometheus:
      configmap_bin: true
      deployment_exporter: true
